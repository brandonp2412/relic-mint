FROM node:8 as base

WORKDIR /usr/src/app
ADD . .
RUN npm install && npm run build

FROM nginx:alpine
COPY --from=base /usr/src/app/dist /usr/share/nginx/html
COPY default.conf /etc/nginx/conf.d/default.conf
EXPOSE 80