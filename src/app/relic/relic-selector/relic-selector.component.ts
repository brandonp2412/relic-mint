import {Component, ViewChild, AfterViewInit} from '@angular/core';
import {
  MatDialogRef,
  MatSnackBar,
  MatPaginator,
  MatSort
} from '@angular/material';
import {Relic} from '../../relic/relic.model';
import {RelicService} from '../../relic/relic.service';
import {merge, Observable} from 'rxjs';
import {startWith, switchMap, map} from 'rxjs/operators';
import {ApiResponseEntities} from '../../api/api-response-entities';
import {SelectionModel} from '@angular/cdk/collections';
import {RelicColors} from '../relic-colors';

@Component({
  selector: 'app-relic-selector',
  templateUrl: './relic-selector.component.html',
  styleUrls: ['./relic-selector.component.scss']
})
export class RelicSelectorComponent implements AfterViewInit {
  relics: Relic[] = [];
  displayedColumns = ['select', 'era', 'name', 'rareDrop'];
  total = 0;
  search: string;
  eraColors = RelicColors.era();
  selection = new SelectionModel<Relic>(true, []);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private relicService: RelicService,
    private snackBar: MatSnackBar,
    private dialogRef: MatDialogRef<Relic>
  ) {}

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => this.getRelics()),
        map(response => {
          this.total = response.total;
          return response.entities;
        })
      )
      .subscribe(relics => (this.relics = relics));
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.relics.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.relics.forEach(relic => this.selection.select(relic));
  }

  getRelics(): Observable<ApiResponseEntities<Relic>> {
    return this.relicService.query(
      this.sort.active,
      this.sort.direction,
      this.paginator.pageIndex,
      this.search
    );
  }

  selectRelic() {
    this.relicService
      .pushMe(this.selection.selected.map(relic => relic._id))
      .subscribe(() => {
        this.snackBar.open(
          `Added: ${this.selection.selected.length} Relics`,
          null,
          {duration: 3000}
        );
        this.dialogRef.close();
      });
  }

  refresh() {
    this.paginator.pageIndex = 0;
    this.getRelics().subscribe(response => {
      this.total = response.total;
      this.relics = response.entities;
    });
  }
}
