import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelicSelectorComponent } from './relic-selector.component';

describe('RelicSelectorComponent', () => {
  let component: RelicSelectorComponent;
  let fixture: ComponentFixture<RelicSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelicSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelicSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
