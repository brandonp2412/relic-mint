import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {
  MatButtonModule,
  MatIconModule,
  MatInputModule,
  MatCardModule,
  MatTooltipModule,
  MatSnackBarModule,
  MatSelectModule,
  MatDialogModule,
  MAT_DIALOG_DATA,
  MatPaginatorModule,
  MatRadioModule,
  MatTableModule,
  MatSortModule,
  MatCheckboxModule
} from '@angular/material';
import {RelicService} from './relic.service';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {Relic} from './relic.model';
import {RelicEditComponent} from '../relic-edit/relic-edit.component';
import {RelicSelectorComponent} from './relic-selector/relic-selector.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatCardModule,
    MatTooltipModule,
    HttpClientModule,
    MatSnackBarModule,
    MatDialogModule,
    MatSelectModule,
    MatPaginatorModule,
    MatRadioModule,
    MatTableModule,
    MatSortModule,
    MatCheckboxModule
  ],
  declarations: [RelicEditComponent, RelicSelectorComponent],
  providers: [RelicService, {provide: MAT_DIALOG_DATA, useValue: new Relic()}]
})
export class RelicModule {}
