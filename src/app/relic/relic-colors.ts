export class RelicColors {
  public static era() {
    return {
      Lith: '#fff5ef',
      Meso: '#ededed',
      Neo: '#f4f4f4',
      Axi: '#fff4dd'
    };
  }
}
