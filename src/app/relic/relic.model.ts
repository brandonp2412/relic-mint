export class Relic {
  _id: string;
  era: string;
  name: string;
  rareDrop: string;
  minSell: number;
  maxBuy: number;
}
