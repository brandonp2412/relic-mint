import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Relic} from './relic.model';
import {Observable} from 'rxjs';
import {ApiService} from '../api/api.service';
import {ApiEditResponse} from '../api/api-edit-response';
import {ApiResponseEntities} from '../api/api-response-entities';

type ArrayResponse = ApiResponseEntities<Relic>;

@Injectable({
  providedIn: 'root'
})
export class RelicService extends ApiService<Relic> {
  resourceUrl = environment.api + '/relic';

  constructor(private http: HttpClient) {
    super();
  }

  find(id: string): Observable<Relic> {
    throw new Error('Method not implemented.');
  }

  create(relic: Relic): Observable<ApiEditResponse> {
    return this.http.post<ApiEditResponse>(this.resourceUrl, relic);
  }

  update(relic: Relic): Observable<ApiEditResponse> {
    return this.http.put<ApiEditResponse>(this.resourceUrl, relic);
  }

  query(
    sort: string,
    order: string,
    page: number,
    search: string
  ): Observable<ArrayResponse> {
    return this.http.get<ArrayResponse>(
      `${
        this.resourceUrl
      }?sort=${sort}&order=${order}&page=${page}&search=${search}`
    );
  }

  remove(id: string): Observable<any> {
    return this.http.delete(`${this.resourceUrl}/${id}`);
  }

  queryMy(_params: any): Observable<ArrayResponse> {
    return this.http.get<ArrayResponse>(this.resourceUrl + '/my', {
      params: _params
    });
  }

  pushMe(ids: string[]): Observable<ApiEditResponse> {
    return this.http.patch<ApiEditResponse>(`${this.resourceUrl}/push-me`, ids);
  }

  popMe(id: string): Observable<ApiEditResponse> {
    return this.http.patch<ApiEditResponse>(
      `${this.resourceUrl}/${id}/pop-me`,
      null
    );
  }
}
