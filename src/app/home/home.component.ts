import {Component, ViewChild, AfterViewInit} from '@angular/core';
import {RelicService} from '../relic/relic.service';
import {Relic} from '../relic/relic.model';
import {MatSort, MatPaginator} from '@angular/material';
import {map, startWith, switchMap} from 'rxjs/operators';
import {merge, Observable} from 'rxjs';
import {ApiResponseEntities} from '../api/api-response-entities';
import {RelicColors} from '../relic/relic-colors';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements AfterViewInit {
  displayedColumns = ['era', 'name', 'rareDrop', 'minSell', 'maxBuy'];
  relics: Relic[] = [];
  total = 0;
  search: string;
  eraColors = RelicColors.era();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private relicService: RelicService) {}

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => this.getRelics()),
        map(response => {
          this.total = response.total;
          return response.entities;
        })
      )
      .subscribe(relics => (this.relics = relics));
  }

  getRelics(): Observable<ApiResponseEntities<Relic>> {
    return this.relicService.query(
      this.sort.active,
      this.sort.direction,
      this.paginator.pageIndex,
      this.search
    );
  }

  refresh() {
    this.paginator.pageIndex = 0;
    this.getRelics().subscribe(response => {
      this.total = response.total;
      this.relics = response.entities;
    });
  }
}
