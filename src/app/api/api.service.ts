import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ApiEditResponse} from './api-edit-response';
import {ApiResponseEntities} from './api-response-entities';

@Injectable({
  providedIn: 'root'
})
export abstract class ApiService<Entity> {
  abstract query(
    sort: string,
    order: string,
    page: number,
    search: string
  ): Observable<ApiResponseEntities<Entity>>;

  abstract find(id: string): Observable<Entity>;

  abstract create(relic: Entity): Observable<ApiEditResponse>;

  abstract update(relic: Entity): Observable<ApiEditResponse>;

  abstract remove(id: string): Observable<ApiEditResponse>;
}
