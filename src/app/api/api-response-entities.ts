export interface ApiResponseEntities<Entity> {
  total: number;
  entities: Entity[];
}
