import {User} from '../auth/user';

export interface ApiAuthResponse {
  token: string;
  user: User;
}
