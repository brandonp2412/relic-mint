export interface ApiEditResponse {
  n: number;
  nModified: number;
  ok: number;
}
