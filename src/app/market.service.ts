import {Injectable} from '@angular/core';
import {environment} from '../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ApiEditResponse} from './api/api-edit-response';

@Injectable({
  providedIn: 'root'
})
export class MarketService {
  resourceUrl = environment.api + '/market';

  constructor(private http: HttpClient) {}

  valueRelics(): Observable<ApiEditResponse> {
    return this.http.patch<ApiEditResponse>(this.resourceUrl, null);
  }
}
