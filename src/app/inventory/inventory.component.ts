import {Component, AfterViewInit, ViewChild} from '@angular/core';
import {Relic} from '../relic/relic.model';
import {MatSort, MatSnackBar, MatDialog, MatPaginator} from '@angular/material';
import {merge} from 'rxjs';
import {startWith, switchMap, map} from 'rxjs/operators';
import {RelicService} from '../relic/relic.service';
import {RelicSelectorComponent} from '../relic/relic-selector/relic-selector.component';
import {MarketService} from '../market.service';
import {RelicColors} from '../relic/relic-colors';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.scss']
})
export class InventoryComponent implements AfterViewInit {
  displayedColumns = [
    'era',
    'name',
    'rareDrop',
    'minSell',
    'maxBuy',
    'buttons'
  ];
  relics: Relic[] = [];
  total = 0;
  search: string;
  eraColors = RelicColors.era();

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private relicService: RelicService,
    private marketService: MarketService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) {}

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => this.getRelics()),
        map(response => {
          this.total = response.total;
          return response.entities;
        })
      )
      .subscribe(relics => (this.relics = relics));
  }

  valueRelics() {
    this.marketService.valueRelics().subscribe(response => {
      this.showSnackBar(`Valued: ${response.nModified} Relics.`);
      this.refresh();
    });
  }

  openSelectRelic() {
    const dialogRef = this.dialog.open(RelicSelectorComponent, {
      width: '500px'
    });
    dialogRef.afterClosed().subscribe(() => this.refresh());
  }

  getRelics() {
    return this.relicService.queryMy({
      sort: this.sort.active,
      order: this.sort.direction,
      page: this.paginator.pageIndex,
      search: this.search
    });
  }

  pop(relic: Relic) {
    this.relicService.popMe(relic._id).subscribe(() => {
      this.showSnackBar(`Deleted Relic "${relic.era} ${relic.name}".`);
      this.refresh();
    });
  }

  refresh() {
    this.getRelics().subscribe(response => {
      this.total = response.total;
      this.relics = response.entities;
    });
  }

  showSnackBar(message: string) {
    this.snackBar.open(message, null, {duration: 3000});
  }
}
