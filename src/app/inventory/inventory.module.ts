import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {InventoryRoutingModule} from './inventory-routing.module';
import {InventoryComponent} from './inventory.component';
import {
  MatToolbarModule,
  MatIconModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatButtonModule,
  MatDialogModule,
  MatRadioModule,
  MatTooltipModule,
  MatInputModule,
  MatGridListModule
} from '@angular/material';
import {FormsModule} from '@angular/forms';
import {RelicSelectorComponent} from '../relic/relic-selector/relic-selector.component';

@NgModule({
  imports: [
    CommonModule,
    InventoryRoutingModule,
    FormsModule,
    MatToolbarModule,
    MatIconModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatButtonModule,
    MatDialogModule,
    MatRadioModule,
    MatTooltipModule,
    MatInputModule,
    MatGridListModule
  ],
  declarations: [InventoryComponent],
  entryComponents: [RelicSelectorComponent]
})
export class InventoryModule {}
