import {Component, AfterViewInit, ViewChild} from '@angular/core';
import {Relic} from '../relic/relic.model';
import {MatPaginator, MatSort, MatDialog, MatSnackBar} from '@angular/material';
import {merge, Observable} from 'rxjs';
import {startWith, switchMap, map} from 'rxjs/operators';
import {ApiResponseEntities} from '../api/api-response-entities';
import {RelicService} from '../relic/relic.service';
import {RelicEditComponent} from '../relic-edit/relic-edit.component';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements AfterViewInit {
  displayedColumns = ['era', 'name', 'rareDrop', 'buttons'];
  relics: Relic[] = [];
  total = 0;
  search: string;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private relicService: RelicService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) {}

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => this.getRelics()),
        map(response => {
          this.total = response.total;
          return response.entities;
        })
      )
      .subscribe(relics => (this.relics = relics));
  }

  getRelics(): Observable<ApiResponseEntities<Relic>> {
    return this.relicService.query(
      this.sort.active,
      this.sort.direction,
      this.paginator.pageIndex,
      this.search
    );
  }

  openEdit(relic = new Relic()) {
    const dialogRef = this.dialog.open(RelicEditComponent, {
      width: '500px',
      data: relic
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getRelics().subscribe(response => (this.relics = response.entities));
    });
  }

  remove(relic: Relic) {
    this.relicService.remove(relic._id).subscribe(() => {
      this.snackBar.open(`Deleted ${relic.era} ${relic.name}`, null, {
        duration: 3000
      });
      this.getRelics().subscribe(response => (this.relics = response.entities));
    });
  }

  refresh() {
    this.paginator.pageIndex = 0;
    this.getRelics().subscribe(response => {
      this.total = response.total;
      this.relics = response.entities;
    });
  }
}
