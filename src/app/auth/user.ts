export interface User {
  roles: string[];
  email: string;
  fullname: string;
  createdAt: Date;
  isAdmin: boolean;
}
