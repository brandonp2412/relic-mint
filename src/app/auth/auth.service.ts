import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';

import {TokenStorage} from './token.storage';
import {environment} from '../../environments/environment';
import {User} from './user';
import {ApiAuthResponse} from '../api/api-auth-response';

@Injectable()
export class AuthService {
  private resourceUrl = environment.api + '/auth';

  constructor(private http: HttpClient, private token: TokenStorage) {}

  public $userSource = new Subject<any>();

  login(email: string, password: string): Observable<any> {
    return Observable.create(observer => {
      this.http
        .post(this.resourceUrl + '/login', {
          email,
          password
        })
        .subscribe((data: ApiAuthResponse) => {
          observer.next({user: data.user});
          this.setUser(data.user);
          this.token.saveToken(data.token);
          observer.complete();
        });
    });
  }

  register(
    fullname: string,
    email: string,
    password: string,
    repeatPassword: string
  ): Observable<any> {
    return Observable.create(observer => {
      this.http
        .post(this.resourceUrl + '/register', {
          fullname,
          email,
          password,
          repeatPassword
        })
        .subscribe((data: any) => {
          observer.next({user: data.user});
          this.setUser(data.user);
          this.token.saveToken(data.token);
          observer.complete();
        });
    });
  }

  setUser(user: User): void {
    if (user) user.isAdmin = user.roles.indexOf('admin') > -1;
    this.$userSource.next(user);
    (<any>window).user = user;
  }

  getUser(): Observable<User> {
    return this.$userSource.asObservable();
  }

  me(): Observable<ApiAuthResponse> {
    return Observable.create(observer => {
      const tokenVal = this.token.getToken();
      if (!tokenVal) return observer.complete();
      this.http.get(this.resourceUrl + '/me').subscribe((data: any) => {
        observer.next({user: data.user});
        this.setUser(data.user);
        observer.complete();
      });
    });
  }

  signOut(): void {
    this.token.signOut();
    this.setUser(null);
    delete (<any>window).user;
  }
}
