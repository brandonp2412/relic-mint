import {Injectable} from '@angular/core';
import {Relic} from '../relic/relic.model';
import {HttpClient} from '@angular/common/http';
import {ApiEditResponse} from '../api/api-edit-response';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {ApiResponseEntities} from '../api/api-response-entities';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private resourceUrl = environment.api + '/user';

  constructor(private http: HttpClient) {}

  getRelics(sort: string, order: string): Observable<Relic[]> {
    return this.http.get<Relic[]>(
      this.resourceUrl + `/relic?sort=${sort}&order=${order}`
    );
  }

  pushRelic(relic: Relic): Observable<ApiEditResponse> {
    return this.http.put<ApiEditResponse>(
      this.resourceUrl + '/relic/push',
      relic
    );
  }

  popRelic(relic: Relic): Observable<ApiEditResponse> {
    return this.http.put<ApiEditResponse>(
      this.resourceUrl + '/relic/pop',
      relic
    );
  }
}
