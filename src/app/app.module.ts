import {BrowserModule} from '@angular/platform-browser';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {LoadingBarHttpClientModule} from '@ngx-loading-bar/http-client';

import {SharedModule} from './shared/shared.module';
import {AuthModule} from './auth/auth.module';

import {AppComponent} from './app.component';
import {AdminModule} from './admin/admin.module';
import {AuthHeaderInterceptor} from './interceptors/header.interceptor';
import {CatchErrorInterceptor} from './interceptors/http-error.interceptor';

import {AppRoutingModule} from './app-routing/app-routing.module';
import {HeaderComponent} from './header/header.component';
import {HomeComponent} from './home/home.component';
import {
  MatButtonModule,
  MatIconModule,
  MatTableModule,
  MatSortModule,
  MatTooltipModule,
  MatPaginatorModule
} from '@angular/material';
import {RelicModule} from './relic/relic.module';
import {RelicEditComponent} from './relic-edit/relic-edit.component';
import {InventoryModule} from './inventory/inventory.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [AppComponent, HeaderComponent, HomeComponent],
  imports: [
    BrowserModule,
    NoopAnimationsModule,
    HttpClientModule,
    RouterModule,
    SharedModule,
    RelicModule,
    AuthModule,
    AdminModule,
    AppRoutingModule,
    InventoryModule,
    MatButtonModule,
    MatIconModule,
    MatTableModule,
    MatSortModule,
    MatTooltipModule,
    MatPaginatorModule,
    LoadingBarHttpClientModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthHeaderInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CatchErrorInterceptor,
      multi: true
    }
  ],
  entryComponents: [RelicEditComponent],
  bootstrap: [AppComponent]
})
export class AppModule {}
