import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {RelicEditComponent} from './relic-edit.component';

describe('RelicEditComponent', () => {
  let component: RelicEditComponent;
  let fixture: ComponentFixture<RelicEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RelicEditComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelicEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
