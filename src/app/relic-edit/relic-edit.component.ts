import {Component, OnInit, Inject} from '@angular/core';
import {MatSnackBar, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {HttpErrorResponse} from '@angular/common/http';
import {Relic} from '../relic/relic.model';
import {RelicService} from '../relic/relic.service';

@Component({
  selector: 'app-relic-edit',
  templateUrl: './relic-edit.component.html',
  styleUrls: ['./relic-edit.component.scss']
})
export class RelicEditComponent implements OnInit {
  relic: Relic;

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: Relic,
    public dialogRef: MatDialogRef<Relic>,
    private service: RelicService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.relic = this.data;
  }

  getNameError() {}

  submit() {
    this.relic._id ? this.update() : this.create();
  }

  create() {
    this.service.create(this.relic).subscribe(
      () => {
        this.snackBar.open(
          `Created Relic "${this.relic.era} ${this.relic.name}"`,
          null,
          {
            duration: 3000
          }
        );
        this.dialogRef.close();
      },
      (httpError: HttpErrorResponse) =>
        this.snackBar.open(httpError.error.message, null, {duration: 3000})
    );
  }

  update() {
    this.service.update(this.relic).subscribe(
      () => {
        this.snackBar.open(
          `Updated relic "${this.relic.era} ${this.relic.name}"`,
          null,
          {
            duration: 3000
          }
        );
        this.dialogRef.close();
      },
      (httpError: HttpErrorResponse) =>
        this.snackBar.open(httpError.error.message, null, {duration: 3000})
    );
  }
}
